/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author FREDY GUSTAVO
 */


public class Conexion {

    public static Connection cnx = null;

    public static Connection obtener() throws SQLException, ClassNotFoundException{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                cnx = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/papeleria", "root", "");
                System.out.println("Conexion Exitosa");
            }catch(SQLException ex){
                throw new SQLException(ex);
            }catch(ClassNotFoundException e){
                throw new ClassNotFoundException(e.getMessage());
            }
        return cnx;
    }
}
