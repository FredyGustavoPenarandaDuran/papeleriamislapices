/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import javax.swing.JOptionPane;

/**
 *
 * @author FREDY GUSTAVO
 */
public class Producto {

    private String nombre;
    private Integer codigo;
    private String descripcion;
    private Integer valor;
    private Integer cantidadDisponible;
    private String categoriaProducto;

    public Producto() {
    }

    public Producto(String nombre, Integer codigo, String descripcion, Integer valor, Integer cantidadDisponible, String categoriaProducto) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
        this.cantidadDisponible = cantidadDisponible;
        this.categoriaProducto = categoriaProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setCantidadDisponible(Integer cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public String getCategoriaProducto() {
        return categoriaProducto;
    }

    public void setCategoriaProducto(String categoriaProducto) {
        this.categoriaProducto = categoriaProducto;
    }

}
