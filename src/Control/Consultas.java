/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Modelo.Producto;
import Servicios.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author FREDY GUSTAVO
 */
public class Consultas {

    public static void guardarProducto(Producto pro) {
        try {
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("INSERT INTO `productos`(`nombre`, `codigo`, `descripcion`, `valor`, `cantidadDisponible`, `categoriaProducto`) "
                    + "VALUES (?, ?, ?, ?, ?,?)");
            consulta.setString(1, pro.getNombre());
            consulta.setInt(2, pro.getCodigo());
            consulta.setString(3, pro.getDescripcion());
            consulta.setInt(4, pro.getValor());
            consulta.setInt(5, pro.getCantidadDisponible());
            consulta.setString(6, pro.getCategoriaProducto());
            consulta.executeUpdate();
            JOptionPane.showMessageDialog(null, "¡Se han guardado los datos con éxito!");

        } catch (SQLException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Ya existe un producto con ese código. \n"+ex);

        }
    }

    public static void eliminarProducto(Integer codigoPro) {
        try {

            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("DELETE FROM productos WHERE codigo= '" + codigoPro + "'");
            consulta.executeUpdate();
            JOptionPane.showMessageDialog(null, "¡Se ha eliminado el producto con éxito!");

        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static void actualizarProducto(Producto pro) {

        try {
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("UPDATE productos SET nombre=?, codigo=?, descripcion=?, valor=?, cantidadDisponible=?, categoriaProducto=?"
                    + "WHERE codigo = '" + pro.getCodigo() + "'");
            consulta.setString(1, pro.getNombre());
            consulta.setInt(2, pro.getCodigo());
            consulta.setString(3, pro.getDescripcion());
            consulta.setInt(4, pro.getValor());
            consulta.setInt(5, pro.getCantidadDisponible());
            consulta.setString(6, pro.getCategoriaProducto());
            consulta.executeUpdate();
            JOptionPane.showMessageDialog(null, "¡Los datos del producto se han actualizado con éxito!");

        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Producto buscarPorNombre(String nombrePro) {
        Producto pro = new Producto();
        try {

            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;

            consulta = conexion.prepareStatement("SELECT * FROM productos WHERE nombre='" + nombrePro + "'");

            ResultSet rs = consulta.executeQuery();

            if (rs.next()) {
                pro.setNombre(rs.getString("nombre"));
                pro.setCodigo(rs.getInt("codigo"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setValor(rs.getInt("valor"));
                pro.setCantidadDisponible(rs.getInt("cantidadDisponible"));
                pro.setCategoriaProducto(rs.getString("categoriaProducto"));
            }

            return pro;

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);

        }
        return pro;

    }

    public static void Pedir(String nombrePro, Integer cantidad) throws SQLException, ClassNotFoundException {
        Producto pro = buscarPorNombre(nombrePro);
        try {
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta1;
            consulta1 = conexion.prepareStatement("SELECT cantidadDisponible FROM productos WHERE codigo = '" + pro.getCodigo() + "'");
            ResultSet rs = consulta1.executeQuery();

            if (rs.next()) {
                pro.setCantidadDisponible(rs.getInt("cantidadDisponible"));
            }

        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Integer cantidadFinal = pro.getCantidadDisponible() + cantidad;

        Connection conexion = Conexion.obtener();
        PreparedStatement consulta;
        consulta = conexion.prepareStatement("UPDATE productos SET cantidadDisponible= '" + cantidadFinal + "' WHERE codigo = " + pro.getCodigo() + "");
        consulta.executeUpdate();

        JOptionPane.showMessageDialog(null, "-- Se ha realizado el pedido con exito --");

    }

    public static Integer actualizarCantidadDisponible(String nombrePro) {
        Producto pro = buscarPorNombre(nombrePro);
        try {
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta1;
            consulta1 = conexion.prepareStatement("SELECT cantidadDisponible FROM productos WHERE codigo = '" + pro.getCodigo() + "'");
            ResultSet rs = consulta1.executeQuery();

            if (rs.next()) {
                pro.setCantidadDisponible(rs.getInt("cantidadDisponible"));
            }

        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pro.getCantidadDisponible();
    }

    public static void Vender(String nombrePro, int cantidad) throws SQLException, ClassNotFoundException {
        Producto pro = buscarPorNombre(nombrePro);
        try {
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta1;
            consulta1 = conexion.prepareStatement("SELECT cantidadDisponible FROM productos WHERE codigo = '" + pro.getCodigo() + "'");
            ResultSet rs = consulta1.executeQuery();

            if (rs.next()) {
                pro.setCantidadDisponible(rs.getInt("cantidadDisponible"));
            }

        } catch (SQLException ex) {

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        Integer cantidadFinal = pro.getCantidadDisponible() - cantidad;
        if (cantidadFinal < 5) {

            JOptionPane.showMessageDialog(null, "No hay stock disponible para realizar la venta (" + cantidad + ").");

        } else {

            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("UPDATE productos SET cantidadDisponible= '" + cantidadFinal + "' WHERE codigo = " + pro.getCodigo() + "");
            consulta.executeUpdate();

            JOptionPane.showMessageDialog(null, "-- Se ha realizado el la venta con exito --");
        }

    }

    public ArrayList<Producto> getProducto() throws SQLException, ClassNotFoundException {

        ArrayList<Producto> listaProductos = new ArrayList<>();


        Connection conexion = Conexion.obtener();
        PreparedStatement consulta;
        consulta = conexion.prepareStatement("SELECT * FROM `productos`");

        ResultSet rs = consulta.executeQuery();
        

        while (rs.next()) {
            Producto pro = new Producto();
            pro.setNombre(rs.getString("nombre"));
            pro.setCodigo(rs.getInt("codigo"));
            pro.setDescripcion(rs.getString("descripcion"));
            pro.setValor(rs.getInt("valor"));
            pro.setCantidadDisponible(rs.getInt("cantidadDisponible"));
            pro.setCategoriaProducto(rs.getString("categoriaProducto"));

            listaProductos.add(pro);
            
        }
        return listaProductos;

    }
}
